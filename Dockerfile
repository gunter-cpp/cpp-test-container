FROM alpine:3.20.1

RUN apk update
RUN apk add git g++ cmake make gcovr

# installing mpdecimal libarary
COPY "./mpdecimal" "./mpdecimal"
RUN cd ./mpdecimal && ./configure && make && make install

CMD ["sh"]